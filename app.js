var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');

// IMPORT ROUTES
var indexRouter = require('./routers/index');
var userRouter = require('./routers/user.routers');
var rolRouter = require('./routers/rol.routers');
var preusuarioRouter = require('./routers/preusuario.routers');
var multimedia = require('./routers/multimedia.routers');
var event = require('./routers/event.routers');
var poster = require('./routers/poster.routers');
//var messageRouter = require('./routers/messages.route');


// IMPORT DB CONNECTION MANAGER
const dbManager = require ("./database/db.manager");

var app = express();
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Set the routing routes to the each script
app.use('/', indexRouter);
app.use('/user', userRouter);
app.use('/rol', rolRouter);
app.use('/pre', preusuarioRouter);
app.use('/img', multimedia);
app.use('/event', event);
app.use('/poster', poster);
/**
 * Testing the connection to the database and recreate the models if the tables doesn´t exists  
 */
dbManager.sequelizeConnection.authenticate()
  .then(() => {
    console.log('****Connection has been established successfully.****');
    // recreate the models if the tables doesn´t exists
    dbManager.sequelizeConnection.sync().then(() => {
        console.log("Database Synced");
      });

  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = app;


