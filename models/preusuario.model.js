module.exports = (sequelize, Sequelize) =>{
    const PreUsuario = sequelize.define ("PreUsuario", {
        idPreUsuario: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        idUsuario: Sequelize.STRING,
        nombres: Sequelize.STRING,
        apellidos: Sequelize.STRING,
        correo: Sequelize.STRING,
        contrasena: Sequelize.STRING,
        rol: Sequelize.STRING,
        fechaIni: Sequelize.DATE,
        fechafin: Sequelize.DATE
    }, {
        tableName: "preusuario"
    });
    return PreUsuario;
}
