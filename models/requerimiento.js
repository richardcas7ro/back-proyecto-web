module.exports = (sequelize, Sequelize) =>{
    const preregistro = sequelize.define ("preregistro", {
        idUser: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nombreUsuario: Sequelize.STRING,
        contrasena: Sequelize.STRING,
        nombres: Sequelize.STRING,
        apellidos: Sequelize.STRING,
        correo: Sequelize.STRING,
        Rol: Sequelize.STRING
    }, {
        tableName: "preregistro"
    });
    return preregistro;
}
