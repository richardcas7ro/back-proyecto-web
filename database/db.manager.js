//IMPORT SEQUELIZE
const Sequelize = require("sequelize");
//IMPORT SEQUELIZE CONNECTION
const sequelizeConnection = require('../database/db.connection.js');
//IMPORT MODELS
const AprovadoresModel = require("../models/aprobadores.model");
const CalificacionesModel = require("../models/calificaciones.model");
const CalificacionPosterModel = require("../models/calificacionposter.model");
const MultimediaModel = require("../models/multimedia.model");
const PerfilModel = require("../models/perfil.model");
const PosterModel = require("../models/poster.model");
const PosterMultimediaModel = require("../models/postermultimedia.model");
const PosterUsuarioModel = require("../models/posterusuario.model");
const RolesModel = require("../models/roles.model");
const RolPerfilModel = require("../models/rolPerfil.model");
const UserModel = require("../models/user.model");
const UsuarioAprobacionModel = require("../models/usuarioaprobacion.model");
const EventModel = require("../models/event.model");
const PosterEventModel = require("../models/eventoposter.model");
const PreUsuarioModel = require("../models/preusuario.model");

//INITIALIZE MODELS
const	Aprovadores	=	AprovadoresModel 		(sequelizeConnection, Sequelize); 
const	Calificaciones	=	CalificacionesModel 		(sequelizeConnection, Sequelize);
const	CalificacionPoster	=	CalificacionPosterModel 		(sequelizeConnection, Sequelize);
const	Multimedia	=	MultimediaModel 		(sequelizeConnection, Sequelize);
const	Perfil	=	PerfilModel 		(sequelizeConnection, Sequelize);
const	Poster	=	PosterModel 		(sequelizeConnection, Sequelize);
const	PosterMultimedia	=	PosterMultimediaModel 		(sequelizeConnection, Sequelize);
const	PosterUsuario	=	PosterUsuarioModel 		(sequelizeConnection, Sequelize);
const	Roles	=	RolesModel 		(sequelizeConnection, Sequelize);
const	RolPerfil	=	RolPerfilModel 		(sequelizeConnection, Sequelize);
const	User	=	UserModel 		(sequelizeConnection, Sequelize);
const	UsuarioAprobacion	=	UsuarioAprobacionModel 		(sequelizeConnection, Sequelize);
const	Event	=	EventModel 		(sequelizeConnection, Sequelize);
const	EventoPoster	=	PosterEventModel 		(sequelizeConnection, Sequelize);
const	UsuarioPre	=	PreUsuarioModel 		(sequelizeConnection, Sequelize);


//CREATE RELATIONS BETWEEN MODELS  
Roles.hasMany( RolPerfil , { foreignKey: 'idRol', sourceKey: 'idRoles' } );
Perfil.hasMany( RolPerfil , { foreignKey: 'idPerfil', sourceKey: 'idPerfil' });
Roles.hasMany( User , { foreignKey: 'idRol', sourceKey: 'idRoles' });
User.hasMany( UsuarioAprobacion , { foreignKey: 'idUsuario', sourceKey: 'idUser' });
Aprovadores.hasMany( UsuarioAprobacion , { foreignKey: 'idAprobacion', sourceKey: 'idAprobadores' });
User.hasMany( PosterUsuario , { foreignKey: 'idUsuario', sourceKey: 'idUser' });
Poster.hasMany( PosterUsuario , { foreignKey: 'idPoster', sourceKey: 'idPoster' });
Poster.hasMany( PosterMultimedia , { foreignKey: 'idPoster', sourceKey: 'idPoster' });
Multimedia.hasMany( PosterMultimedia , { foreignKey: 'idMultimedia', sourceKey: 'idMultimedia' });
Poster.hasMany( CalificacionPoster , { foreignKey: 'idPoster', sourceKey: 'idPoster' });
Calificaciones.hasMany( CalificacionPoster , { foreignKey: 'idCalificacion', sourceKey: 'idCalificaciones' });
Multimedia.hasMany( CalificacionPoster , { foreignKey: 'idMultimedia', sourceKey: 'idMultimedia' });
Poster.hasMany( EventoPoster , { foreignKey: 'idPoster', sourceKey: 'idPoster' });
EventoPoster.belongsTo(Poster, { foreignKey: 'idPoster', sourceKey: 'idPoster' });
Event.hasMany( EventoPoster , { foreignKey: 'idEvento', sourceKey: 'idEvent' });

//GROUP MODELS
const models = {
  Aprovadores	:	Aprovadores	,
  Calificaciones	:	Calificaciones	,
  CalificacionPoster	:	CalificacionPoster	,
  Multimedia	:	Multimedia	,
  Perfil	:	Perfil	,
  Poster	:	Poster	,
  PosterMultimedia	:	PosterMultimedia	,
  PosterUsuario	:	PosterUsuario	,
  Roles	:	Roles	,
  RolPerfil	:	RolPerfil	,
  User	:	User	,
  UsuarioAprobacion	:	UsuarioAprobacion	,
  Event: Event,
  EventoPoster : EventoPoster ,
  UsuarioPre : UsuarioPre
};


/**
 * Create object to manage the models and database
 */
const db = {
    ...models,
    sequelizeConnection
};
  
// EXPORT CONSTANT DB
module.exports = db;
