const dbManager = require ('../database/db.manager');
const Sequelize = require("sequelize");

/**
 * 
 * Creation of a rol
 * 
 */
async function createImg (req, res) {
    
    res.status(200).send({
        message: "Some error occurred"
    });
    var r = req.query;
    var src = req.body;

    const Op = Sequelize.Op
    // CHECK IF THE REQUEST BODY IS EMPTY
    if (!req.body) {
        res.status(400).send({
          message: "Request body is empty!!!!"
        });
        return;
    }

    // CREATING THE OBJECT TO PERSIST
    const newMultiObject = {      
        nombreMultimedia:r.nombreMultimedia,
        ruta: src.nombreUsuario,
        formato: r.formato,
        ubicacion: r.descMultimedia,
        fechaIni:new Date(),
        fechafin:new Date()
    }

        console.log("salida1");
        // EXECUTING THE CREATE QUERY - INSERT THE OBJECT INTO DATABASE 
        dbManager.Multimedia.create(newMultiObject).then (
            data => {
                res.send ('OK');
            }
        ).catch (
            e => {
                // Print error on console
                console.log("_____________mal");
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
            }
        );    
}


/**
 * 
 * Select all img
 * 
 */
async function getall (req, res) {
    const Op = Sequelize.Op

    var fec = new Date();
    var es = req.query;
    //VALIDATE PERC
    try{
    const eventos =  await dbManager.Multimedia.findAll ({
        where: {
                fechaIni: {
                    [Op.lte]: fec
                },
                fechafin: {
                    [Op.gte]: fec
                }
        }
    });
    console.log(Object.entries(eventos).length);
    if (Object.entries(eventos).length === 0|| eventos == null){
        res.status(404).send(
            {
                message:"not found"
            })
       }else{
           res.json(eventos);     
       }

    res.json(eventos);
    } catch (e) {      
            // Print error on console
            console.log(e);
            // Send error message as a response 
            res.status(500).send({
                message: "Some error occurred"
            });
}       
}

exports.createImg = createImg; 
exports.getall = getall; 