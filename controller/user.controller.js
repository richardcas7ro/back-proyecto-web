const dbManager = require ('../database/db.manager');
const Sequelize = require("sequelize");
/**
 * 
 * Creation of an user
 * 
 */
async function createUser (req, res) {
    
    // CHECK IF THE REQUEST BODY IS EMPTY
    if (!req.body) {
        res.status(400).send({
          message: "Request body is empty!!!!"
        });
        return;
    }

    const Op = Sequelize.Op
    var create = true;
    
    var es = req.body;
    console.log(es);
    
    // VALIDATE UNIQUE USER THE DATES
    var usersC = await dbManager.User.findAll ({
            where: {
                [Op.and]:{
                    nombreUsuario: es.nombreUsuario,
                    fechaIni:{
                        [Op.lte]: es.fechaIni
                    },
                    fechafin:{
                        [Op.gte]: es.fechaIni
                    }
                }
            }
        });
    
        if (Object.entries(usersC).length === 0|| usersC == null){
            usersC = await dbManager.User.findAll ({
                where: {
                    [Op.and]:{
                        nombreUsuario: es.nombreUsuario,
                        fechaIni:{
                            [Op.lte]: es.fechafin
                        },
                        fechafin:{
                            [Op.gte]: es.fechafin
                        }
                    }
                }
            });
            if (Object.entries(usersC).length === 0|| usersC == null){
                usersC = await dbManager.User.findAll ({
                    where: {
                        [Op.and]:{
                            nombreUsuario: es.nombreUsuario,
                            fechaIni:{
                                [Op.gte]: es.fechaIni
                            },
                            fechafin:{
                                [Op.lte]: es.fechafin
                            }
                        }
                    }
                });
                if (Object.entries(usersC).length === 0|| usersC == null){
                }else{  create = false ;
                }
            }else{
                create = false ;
            }
        }else{
            create = false ;
        }
    

    // CREATING THE OBJECT TO PERSIST
    const newUserObject = {      
        nombreUsuario:req.body.nombreUsuario,
        contrasena:req.body.contrasena,
        nombres:req.body.nombres,
        apellidos:req.body.apellidos,
        correo:req.body.correo,
        activo:req.body.activo,
        token:req.body.token,
        fechaIni:req.body.fechaIni,
        fechafin:req.body.fechafin,
        idRol:req.body.idRol
    }
    
    if (create){
    // EXECUTING THE CREATE QUERY - INSERT THE OBJECT INTO DATABASE 
        dbManager.User.create(newUserObject).then (
            data => {
                res.send (data);
            }
        ).catch (
            e => {
                // Print error on console
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
            }
        );
    }else{
        res.status(500).send({
            message: "User " + req.body.nombreUsuario + " already exist"
        });
    }
}

/**
 * GEt all users between two dates
 */
async function findAllUsersByDates (req, res){
    const Op = Sequelize.Op

    console.log(req);
    var ini = new Date(req.query.fechaIni);
    var fin = new Date(req.query.fechafin);
    ini.setHours(ini.getHours() - 5);
    fin.setHours(fin.getHours() - 5);

    console.log(ini.toISOString());

    try {
        //Execute query
        const users = await dbManager.User.findAll ({
            where: {
                [Op.and]:{
                    fechaIni:{
                        [Op.gte]: ini
                    },
                    fechafin:{
                        [Op.lte]: fin
                    }
                }
            }
        });
        
        //Send response
        res.json({
                data: users
        });

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}


/**
 * Get user by id
 */
async function findOneUser (req, res){
    try {
        const { idUser } = req.params;

        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                idUser: idUser
            }
        });
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Get user by name
 */
async function findOneUserName (req, res){
    try {
        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                nombreUsuario: req.query.nombreUsuario
            }
        });
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Get user by name
 */
async function findOneUserName (req, res){
    try {
        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                nombres: req.query.nombres
            }
        });
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Get user by lastname
 */
async function findOneUserLastName (req, res){
    try {
        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                apellidos: req.query.apellidos
            }
        });
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}


/**
 * Get user by username
 */
async function findOneUserbyuserName (req, res){
    try {
        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                nombreUsuario: req.query.nombreUsuario
            }
        });
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Get user by name and date
 */
async function findOneUserNamed (req, res){

    const Op = Sequelize.Op

    console.log(req);
    var ini = new Date(req.query.fechaIni);
    var fin = new Date(req.query.fechafin);
    ini.setHours(ini.getHours() - 5);
    fin.setHours(fin.getHours() - 5);

    try {
        //Execute query
        const user = await dbManager.User.findOne ({
            where: {
                [Op.and]:{
                    nombres: req.query.nombres,
                    fechaIni:{
                        [Op.gte]: ini
                    },
                    fechafin:{
                        [Op.lte]: fin
                    }
                }
            }
        });
    
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Get user by lastname and date
 */
async function findOneUserLastNamed (req, res){
    
    const Op = Sequelize.Op

    var ini = new Date(req.query.fechaIni);
    var fin = new Date(req.query.fechafin);
   // ini.setHours(ini.getHours() - 5);
   // fin.setHours(fin.getHours() - 5);

    console.log(req.query)

    try {
        //Execute query
        const user = await dbManager.User.findOne ({
            where: {
                [Op.and]:{
                    apellidos: req.query.apellidos,
                    fechaIni:{
                        [Op.gte]: ini
                    },
                    fechafin:{
                        [Op.lte]: fin
                    }
                }
            }
        });
    
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }

}


/**
 * Get user by username and date
 */
async function findOneUserbyuserNamed (req, res){
    
    const Op = Sequelize.Op

    var ini = new Date(req.query.fechaIni);
    var fin = new Date(req.query.fechafin);
    //ini.setHours(ini.getHours() - 5);
    //fin.setHours(fin.getHours() - 5);
    console.log(req.query);
    console.log(fin);

    try {
        //Execute query
        const user = await dbManager.User.findOne ({
            where: {
                [Op.and]:{
                    nombreUsuario: req.query.nombreUsuario,
                    fechaIni:{
                        [Op.gte]: ini
                    },
                    fechafin:{
                        [Op.lte]: fin
                    }
                }
            }
        });
    
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}


/**
 * Update user
 */
async function updateUser (req, res){
    try {
        const { idUser } = req.params;

        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                idUser: idUser
            }
        });
        if (req.body.nombreUsuario != null){
            user.nombreUsuario = req.body.nombreUsuario;
        }
        if (req.body.contrasena != null){
            user.contrasena = req.body.contrasena;
        }
        if (req.body.nombres != null){
            user.nombres = req.body.nombres;
        }
        if (req.body.apellidos != null){
            user.apellidos = req.body.apellidos;
        }
        if (req.body.correo != null){
            user.correo = req.body.correo;
        }
        if (req.body.activo != null){
            user.activo = req.body.activo;
        }
        if (req.body.token != null){
            user.token = req.body.token;
        }
        if (req.body.fechaIni != null){
            user.fechaIni = req.body.fechaIni;
        }
        if (req.body.fechafin != null){
            user.fechafin = req.body.fechafin;
        }
        if (req.body.idRol != null){
            user.idRol = req.body.idRol;
        }
        
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Delete an existen user by username
 * @param {*} req 
 * @param {*} res 
 */
function deleteUserByUsername (req, res){ 
                /**
                 * TASK:
                 * IMPLEMENT THE FUNCTION______________________- 
                 */

}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
function deleteAllUsers (req, res){
                /**
                 * TASK:
                 * IMPLEMENT THE FUNCTION______________________- 
                 */
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
function findAllUsersByCreatedDate (req, res){
                /**
                 * TASK:
                 * IMPLEMENT THE FUNCTION______________________- 
                 */
}

/**
 * Get user by id
 */
async function ValidateOneUser (req, res){
    try {
        const { username } = req.params;

        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                username: username
            }
        });
        //Send response
        if ( user == null ){
            res.json(false);
        }else{
            res.json(true);
        }         
    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
    
}

function generatePassword() {
    var length = 30,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

/**
 * Get user by id
 */
async function loginU (req, res){
    const Op = Sequelize.Op

    var ss = req.query;
       
    try{
        // se trae el parametro

        const users = await dbManager.User.findOne({
               where: {
                [Op.and]:{
                    nombreUsuario: ss.username,
                    contrasena: ss.password
                }
               }
           }
       );

        console.log(users);
        if (Object.entries(users).length === 0|| users == null){
        res.status(404).send(
            {
                message:"userandpass not found"
            })
       }else{
           var pss = generatePassword();
           users.token = pss;
           await users.save();
           res.json(users);     
       }

     
       
    
    }catch(error){
        console.log(error,"userandpass not found")
        res.status(500).send(
            {
                message:"userandpass not found"
            }
        )

    }

    
}

/**
 * 
 * Creation of an relation bettwen user and poster
 * 
 */
async function createUserP (req, res) {
    
    // CHECK IF THE REQUEST BODY IS EMPTY
    if (!req.body) {
        res.status(400).send({
          message: "Request body is empty!!!!"
        });
        return;
    }

    const Op = Sequelize.Op
    var create = true;
    
    var es = req.body;
    console.log(es);
    
    // CREATING THE OBJECT TO PERSIST
    const newUserObject = {      
        idPoster:req.body.idPoster,
        idUsuario:req.body.idUsuario,
        idAprobadores:req.body.idAprobadores,
        fechaIni:req.body.fechaIni,
        fechafin:req.body.fechafin,
    }
    
    
    if (create){
    // EXECUTING THE CREATE QUERY - INSERT THE OBJECT INTO DATABASE 
        dbManager.PosterUsuario.create(newUserObject).then (
            data => {
                res.send (data);
            }
        ).catch (
            e => {
                // Print error on console
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
            }
        );
    }else{
        res.status(500).send({
            message: "User " + req.body.nombreUsuario + " already exist"
        });
    }
}


exports.createUserP = createUserP;
exports.createUser = createUser; 
exports.findAllUsersByDates = findAllUsersByDates; 
exports.findOneUser = findOneUser; 
exports.findOneUserName = findOneUserName;
exports.loginU = loginU;
exports.findOneUserbyuserName = findOneUserbyuserName;
exports.findOneUserLastName = findOneUserLastName;
exports.updateUser = updateUser;
exports.deleteUserByUsername = deleteUserByUsername;
exports.deleteAllUsers = deleteAllUsers;
exports.findAllUsersByCreatedDate = findAllUsersByCreatedDate;
exports.ValidateOneUser = ValidateOneUser;
exports.findOneUserNamed = findOneUserNamed;
exports.findOneUserLastNamed = findOneUserLastNamed;
exports.findOneUserbyuserNamed= findOneUserbyuserNamed;