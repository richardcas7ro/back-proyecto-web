const dbManager = require ('../database/db.manager');
const Sequelize = require("sequelize");

/**
 * 
 * Creation of a event
 * 
 */
async function createEve (req, res) {
    const Op = Sequelize.Op
    // CHECK IF THE REQUEST BODY IS EMPTY
    if (!req.body) {
        res.status(400).send({
          message: "Request body is empty!!!!"
        });
        return;
    }

    // CREATING THE OBJECT TO PERSIST
    const newRolObject = {      
        titulo:req.body.titulo,
        descripcion:req.body.descripcion,
        materia:req.body.materia,
        carrera:req.body.carrera,
        grupo:req.body.grupo,
        statusAprob:req.body.statusAprob,
        vistas:req.body.vistas,
        fechaIni:req.body.fechaIni,
        fechafin:req.body.fechafin,
        idAprobador:req.body.idAprobador,
    }
    
    //VALIDATE PERC
    const event =  await dbManager.Poster.findAll ({
        where: {
            [Op.or]: [{
                titulo: newRolObject.titulo,
                fechaIni: {
                    [Op.between]: [new Date(req.body.fechaIni), new Date(req.body.fechafin)]
                }
            }, {
                fechafin: {
                    [Op.between]: [new Date(req.body.fechaIni), new Date(req.body.fechafin)]
                }
            }]
       
        }
        
    }).catch (
        e => {
            // Print error on console
            console.log(e);
            // Send error message as a response 
            res.status(500).send({
                message: "Some error occurred"
            });
        }
    );


    if (event != null ){
        
        // EXECUTING THE CREATE QUERY - INSERT THE OBJECT INTO DATABASE 
        dbManager.Poster.create(newRolObject).then (
            data => {
                res.send (data);
            }
        ).catch (
            e => {
                // Print error on console
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
            }
        );
    }else{
        // Send error message as a response 
        res.status(500).send({
            message: "Existing Rol for this dates"
        });
    }
}

/**
 * 
 * Select a event
 * 
 */
async function get (req, res) {
    const Op = Sequelize.Op
    
    var fec = new Date();
    var es = req.query;
    //VALIDATE PERC
    try{
        const eventos =  await dbManager.Poster.findAll ({
            where: {
                    titulo: es.titulo,
                    fechaIni: {
                        [Op.lte]: fec
                    },
                    fechafin: {
                        [Op.gte]: fec
                    }
            }
        });
        
        if (Object.entries(eventos).length === 0|| eventos == null){
            res.status(500).send({
                message: "Some error occurred"
            });          
        }else{
            res.json(eventos);
        }
      
       
    
    } catch (e) {      
            // Print error on console
            console.log(e);
            // Send error message as a response 
            res.status(500).send({
                message: "Some error occurred"
            });
}

    
}

/**
 * 
 * Select a event
 * 
 */
async function getall (req, res) {
        const Op = Sequelize.Op

        var fec = new Date();
        var es = req.query;
        //VALIDATE PERC
        try{
            
            const eventos =  await dbManager.PosterUsuario.findAll ({
                where: {
                        idUsuario: es.idUsuario,
                        fechaIni: {
                            [Op.lte]: fec
                        },
                        fechafin: {
                            [Op.gte]: fec
                        }
                },
            });
            
            foods = new Array() ;
            for(var i = 0; i < Object.values(eventos).length; i++){      
                foods.push(eventos[i].idPoster);
            }
            

            if (foods.length > 0){
                const eventos2 =  await dbManager.Poster.findAll ({
                    where: {
                            idPoster: {
                                [Op.in]: foods
                            }
                    },
                });

                if (Object.entries(eventos2).length === 0|| eventos2 == null){
                    res.status(500).send({
                        message: "not found"
                    });          
                }else{
                    res.json(eventos2);
                }

            }else{
                res.status(500).send({
                    message: "no found"
                });
            }

        
        } catch (e) {      
                // Print error on console
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
    }        
}


exports.createEve = createEve; 
exports.getall = getall;
exports.get = get;
