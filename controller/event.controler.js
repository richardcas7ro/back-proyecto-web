const dbManager = require ('../database/db.manager');
const Sequelize = require("sequelize");

/**
 * 
 * Creation of a event
 * 
 */
async function createEve (req, res) {
    const Op = Sequelize.Op
    // CHECK IF THE REQUEST BODY IS EMPTY
    if (!req.body) {
        res.status(400).send({
          message: "Request body is empty!!!!"
        });
        return;
    }

    // CREATING THE OBJECT TO PERSIST
    const newRolObject = {      
        nombre:req.body.nombre,
        descripcion:req.body.descripcion,
        fechaIni:req.body.fechaIni,
        fechafin:req.body.fechafin,
        idAprobador:req.body.idAprobador,
    }
    
    //VALIDATE PERC
    const event =  await dbManager.Event.findAll ({
        where: {
            [Op.or]: [{
                nombre: newRolObject.nombre,
                fechaIni: {
                    [Op.between]: [new Date(req.body.fechaIni), new Date(req.body.fechafin)]
                }
            }, {
                fechafin: {
                    [Op.between]: [new Date(req.body.fechaIni), new Date(req.body.fechafin)]
                }
            }]
       
        }
        
    }).catch (
        e => {
            // Print error on console
            console.log(e);
            // Send error message as a response 
            res.status(500).send({
                message: "Some error occurred"
            });
        }
    );


    if (event != null ){
        console.log("salida1");
        // EXECUTING THE CREATE QUERY - INSERT THE OBJECT INTO DATABASE 
        dbManager.Event.create(newRolObject).then (
            data => {
                res.send (data);
            }
        ).catch (
            e => {
                // Print error on console
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
            }
        );
    }else{
        // Send error message as a response 
        res.status(500).send({
            message: "Existing Rol for this dates"
        });
    }
}



/**
 * 
 * Creation of a event
 * 
 */
async function createLink (req, res) {
    const Op = Sequelize.Op
    // CHECK IF THE REQUEST BODY IS EMPTY
    if (!req.body) {
        res.status(400).send({
          message: "Request body is empty!!!!"
        });
        return;
    }

    // CREATING THE OBJECT TO PERSIST
    const newRolObject = {      
        idPoster:req.body.idPoster,
        idEvento:req.body.idEvento,
        fechaIni:req.body.fechaIni,
        fechafin:req.body.fechafin,
    }
    
    //VALIDATE PERC
    const event =  await dbManager.EventoPoster.findAll ({
        where: {
            [Op.or]: [{
                idPoster: newRolObject.idPoster,
                idEvento: newRolObject.idEvento,
                fechaIni: {
                    [Op.between]: [new Date(req.body.fechaIni), new Date(req.body.fechafin)]
                }
            }, {
                fechafin: {
                    [Op.between]: [new Date(req.body.fechaIni), new Date(req.body.fechafin)]
                }
            }]
       
        }
        
    }).catch (
        e => {
            // Print error on console
            console.log(e);
            // Send error message as a response 
            res.status(500).send({
                message: "Some error occurred"
            });
        }
    );


    if (event != null ){
        // EXECUTING THE CREATE QUERY - INSERT THE OBJECT INTO DATABASE 
        dbManager.EventoPoster.create(newRolObject).then (
            data => {
                res.send (data);
            }
        ).catch (
            e => {
                // Print error on console
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
            }
        );
    }else{
        // Send error message as a response 
        res.status(500).send({
            message: "Existing Rol for this dates"
        });
    }
}

/**
 * 
 * Select a event
 * 
 */
async function get (req, res) {
    const Op = Sequelize.Op

    var fec = new Date();
    var es = req.query;
    //VALIDATE PERC
    try{
    const eventos =  await dbManager.Event.findAll ({
        where: {
                nombre: es.nombre,
                fechaIni: {
                    [Op.lte]: fec
                },
                fechafin: {
                    [Op.gte]: fec
                }
        }
    });
    if (Object.entries(eventos).length === 0|| eventos == null){
        res.status(404).send(
            {
                message:"not found"
            })
       }else{
           res.json(eventos);     
       }
    } catch (e) {      
            // Print error on console
            console.log(e);
            // Send error message as a response 
            res.status(500).send({
                message: "Some error occurred"
            });
}

    
}

/**
 * 
 * Select a event
 * 
 */
async function getall (req, res) {
        const Op = Sequelize.Op

        var fec = new Date();
        var es = req.query;
        //VALIDATE PERC
        try{
        const eventos =  await dbManager.Event.findAll ({
            where: {
                    fechaIni: {
                        [Op.lte]: fec
                    },
                    fechafin: {
                        [Op.gte]: fec
                    }
            }
        });

        if (Object.entries(eventos).length === 0|| eventos == null){
            res.status(404).send(
                {
                    message:"not found"
                })
           }else{
               res.json(eventos);     
           }

        res.json(eventos);
        } catch (e) {      
                // Print error on console
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
    }       
}


exports.createEve = createEve; 
exports.createLink = createLink;
exports.getall = getall;
exports.get = get;
