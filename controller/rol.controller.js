const dbManager = require ('../database/db.manager');
const Sequelize = require("sequelize");

/**
 * 
 * Creation of a rol
 * 
 */
async function createRol (req, res) {
    const Op = Sequelize.Op
    // CHECK IF THE REQUEST BODY IS EMPTY
    if (!req.body) {
        res.status(400).send({
          message: "Request body is empty!!!!"
        });
        return;
    }

    // CREATING THE OBJECT TO PERSIST
    const newRolObject = {      
        nombreRol:req.body.nombreRol,
        descripcionRol:req.body.descripcionRol,
        fechaIni:req.body.fechaIni,
        fechafin:req.body.fechafin
    }
    
    var msec = Date.parse(newRolObject.fechaIni); 
    console.log(msec); 
    var msec2 = Date.parse(newRolObject.fechafin); 
    console.log(msec2+ ' '+ new Date(2020, 1, 1)); 
    //VALIDATE PERC
    const roles =  await dbManager.Roles.findAll ({
        where: {
            [Op.or]: [{
                fechaIni: {
                    [Op.between]: [new Date(req.body.fechaIni), new Date(req.body.fechafin)]
                }
            }, {
                fechafin: {
                    [Op.between]: [new Date(req.body.fechaIni), new Date(req.body.fechafin)]
                }
            }]
       
        }
        
    }).catch (
        e => {
            // Print error on console
            console.log(e);
            // Send error message as a response 
            res.status(500).send({
                message: "Some error occurred"
            });
        }
    );

    console.log("salida"+req.body.fechafin);
    console.log(roles);

    if (roles == null && 1==2){
        console.log("salida1");
        // EXECUTING THE CREATE QUERY - INSERT THE OBJECT INTO DATABASE 
        dbManager.Roles.create(newRolObject).then (
            data => {
                res.send (data);
            }
        ).catch (
            e => {
                // Print error on console
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
            }
        );
    }else{
        console.log("salida2");
        // Print error on console
        console.log("Existing Rol for this dates");
        // Send error message as a response 
        res.status(500).send({
            message: "Existing Rol for this dates"
        });
    }
}

/**
 * 
 * Select a rol
 * 
 */
async function selectRol (req, res) {
    const Op = Sequelize.Op

    
    var es = req.query;
    var fec = new Date();
    //VALIDATE PERC
    
        const roles =  await dbManager.Roles.findAll ({
            where: {
                    nombreRol: es.nombreRol,
                    fechaIni: {
                        [Op.lte]: fec
                    },
                    fechafin: {
                        [Op.gte]: fec
                    }
            }
        }).then (
            data => {
                console.log(data);
                res.send (data);
            },
        ).catch (
            e => {
                // Print error on console
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
            }
        );
    
    

    
}



exports.createRol = createRol; 
exports.selectRol = selectRol;
