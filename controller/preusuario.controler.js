const dbManager = require ('../database/db.manager');
const Sequelize = require("sequelize");
/**
 * 
 * Creation of a pre user
 * 
 */
async function createUserPre (req, res) {
    console.log("backend");
    console.log(req.body);
    // CHECK IF THE REQUEST BODY IS EMPTY
    if (!req.body) {
        res.status(400).send({
          message: "Request body is empty!!!!"
        });
        return;
    }

    const Op = Sequelize.Op
    var create = true;
    
    // VALIDATE UNIQUE USER THE DATES
    var usersC = await dbManager.UsuarioPre.findAll ({
            where: {
                [Op.and]:{
                    idUsuario: req.body.idUsuario,
                    fechaIni:{
                        [Op.lte]: req.body.fechaIni
                    },
                    fechafin:{
                        [Op.gte]: req.body.fechaIni
                    }
                }
            }
        });
    
        if (Object.entries(usersC).length === 0|| usersC == null){
            usersC = await dbManager.UsuarioPre.findAll ({
                where: {
                    [Op.and]:{
                        idUsuario: req.body.idUsuario,
                        fechaIni:{
                            [Op.lte]: req.body.fechafin
                        },
                        fechafin:{
                            [Op.gte]: req.body.fechafin
                        }
                    }
                }
            });
            if (Object.entries(usersC).length === 0|| usersC == null){
                usersC = await dbManager.UsuarioPre.findAll ({
                    where: {
                        [Op.and]:{
                            idUsuario: req.body.idUsuario,
                            fechaIni:{
                                [Op.gte]: req.body.fechaIni
                            },
                            fechafin:{
                                [Op.lte]: req.body.fechafin
                            }
                        }
                    }
                });
                if (Object.entries(usersC).length === 0|| usersC == null){
                }else{  create = false ;
                }
            }else{
                create = false ;
            }
        }else{
            create = false ;
        }
    

    // CREATING THE OBJECT TO PERSIST
    const newUserObject = {      
        idUsuario:req.body.idUsuario,
        contrasena:req.body.contrasena,
        nombres:req.body.nombres,
        apellidos:req.body.apellidos,
        correo:req.body.correo,
        rol:req.body.Rol,
        fechaIni:new Date(),
        fechafin:new Date()
    }
    
    if (create){
    // EXECUTING THE CREATE QUERY - INSERT THE OBJECT INTO DATABASE 
        dbManager.UsuarioPre.create(newUserObject).then (
            data => {
                res.send (data);
            }
        ).catch (
            e => {
                // Print error on console
                console.log(e);
                // Send error message as a response 
                res.status(500).send({
                    message: "Some error occurred"
                });
            }
        );
    }else{
        res.status(500).send({
            message: "User " + req.body.idUsuario + " already exist"
        });
    }
}

async function createUserPreOp (req, res) {
    res.status(200).send({
        message: "conected"
    });
}

/**
 * GEt all users 
 */
async function findAllUsers (req, res){
    const Op = Sequelize.Op

    try {
        //Execute query
        
        var users = await dbManager.UsuarioPre.findAll ();
        
        //Send response
        res.json({
                data: users
        });

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Delete a user 
 */
async function deleteUsers (req, res){
    const Op = Sequelize.Op
    try {
        //Execute query
        console.log(req.query)
        
        var es = req.query
       
        var users = await dbManager.UsuarioPre.destroy ({
            where: {
                [Op.and]:{
                    idPreUsuario: es.idPreUsuario,
                }
            }
        });
        
        //Send response
        res.json({
                data: users
        });

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}
deleteUsers

/**
 * GEt all users between two dates
 */
async function findAllUsersByDates (req, res){
    const Op = Sequelize.Op

    console.log(req);
    var ini = new Date(req.query.fechaIni);
    var fin = new Date(req.query.fechafin);
    ini.setHours(ini.getHours() - 5);
    fin.setHours(fin.getHours() - 5);
    console.log(ini.toISOString());

    try {
        //Execute query
        const users = await dbManager.User.findAll ({
            where: {
                [Op.and]:{
                    fechaIni:{
                        [Op.gte]: ini
                    },
                    fechafin:{
                        [Op.lte]: fin
                    }
                }
            }
        });
        
        //Send response
        res.json({
                data: users
        });

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}


/**
 * Get user by id
 */
async function findOneUser (req, res){
    try {
        const { idUser } = req.params;

        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                idUser: idUser
            }
        });
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Get user by name
 */
async function findOneUserName (req, res){
    try {
        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                nombreUsuario: req.query.nombreUsuario
            }
        });
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Get user by name
 */
async function findOneUserName (req, res){
    try {
        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                nombres: req.query.nombres
            }
        });
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Get user by lastname
 */
async function findOneUserLastName (req, res){
    try {
        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                apellidos: req.query.apellidos
            }
        });
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}


/**
 * Get user by username
 */
async function findOneUserbyuserName (req, res){
    try {
        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                nombreUsuario: req.query.nombreUsuario
            }
        });
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Get user by name and date
 */
async function findOneUserNamed (req, res){

    const Op = Sequelize.Op

    console.log(req);
    var ini = new Date(req.query.fechaIni);
    var fin = new Date(req.query.fechafin);
    ini.setHours(ini.getHours() - 5);
    fin.setHours(fin.getHours() - 5);

    try {
        //Execute query
        const user = await dbManager.User.findOne ({
            where: {
                [Op.and]:{
                    nombres: req.query.nombres,
                    fechaIni:{
                        [Op.gte]: ini
                    },
                    fechafin:{
                        [Op.lte]: fin
                    }
                }
            }
        });
    
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Get user by lastname and date
 */
async function findOneUserLastNamed (req, res){
    
    const Op = Sequelize.Op

    var ini = new Date(req.query.fechaIni);
    var fin = new Date(req.query.fechafin);
   // ini.setHours(ini.getHours() - 5);
   // fin.setHours(fin.getHours() - 5);

    console.log(req.query)

    try {
        //Execute query
        const user = await dbManager.User.findOne ({
            where: {
                [Op.and]:{
                    apellidos: req.query.apellidos,
                    fechaIni:{
                        [Op.gte]: ini
                    },
                    fechafin:{
                        [Op.lte]: fin
                    }
                }
            }
        });
    
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }

}


/**
 * Get user by username and date
 */
async function findOneUserbyuserNamed (req, res){
    
    const Op = Sequelize.Op

    var ini = new Date(req.query.fechaIni);
    var fin = new Date(req.query.fechafin);
    //ini.setHours(ini.getHours() - 5);
    //fin.setHours(fin.getHours() - 5);
    console.log(req.query);
    console.log(fin);

    try {
        //Execute query
        const user = await dbManager.User.findOne ({
            where: {
                [Op.and]:{
                    nombreUsuario: req.query.nombreUsuario,
                    fechaIni:{
                        [Op.gte]: ini
                    },
                    fechafin:{
                        [Op.lte]: fin
                    }
                }
            }
        });
    
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}


/**
 * Update user
 */
async function updateUser (req, res){
    try {
        const { idUser } = req.params;

        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                idUser: idUser
            }
        });
        if (req.body.nombreUsuario != null){
            user.nombreUsuario = req.body.nombreUsuario;
        }
        if (req.body.contrasena != null){
            user.contrasena = req.body.contrasena;
        }
        if (req.body.nombres != null){
            user.nombres = req.body.nombres;
        }
        if (req.body.apellidos != null){
            user.apellidos = req.body.apellidos;
        }
        if (req.body.correo != null){
            user.correo = req.body.correo;
        }
        if (req.body.activo != null){
            user.activo = req.body.activo;
        }
        if (req.body.token != null){
            user.token = req.body.token;
        }
        if (req.body.fechaIni != null){
            user.fechaIni = req.body.fechaIni;
        }
        if (req.body.fechafin != null){
            user.fechafin = req.body.fechafin;
        }
        if (req.body.idRol != null){
            user.idRol = req.body.idRol;
        }
        
        //Send response
        res.json(user);

    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
}

/**
 * Delete an existen user by username
 * @param {*} req 
 * @param {*} res 
 */
function deleteUserByUsername (req, res){ 
                /**
                 * TASK:
                 * IMPLEMENT THE FUNCTION______________________- 
                 */

}



/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
function deleteAllUsers (req, res){
                /**
                 * TASK:
                 * IMPLEMENT THE FUNCTION______________________- 
                 */
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
function findAllUsersByCreatedDate (req, res){
                /**
                 * TASK:
                 * IMPLEMENT THE FUNCTION______________________- 
                 */
}

/**
 * Get user by id
 */
async function ValidateOneUser (req, res){
    try {
        const { username } = req.params;

        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                username: username
            }
        });
        //Send response
        if ( user == null ){
            res.json(false);
        }else{
            res.json(true);
        }         
    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
    
}

exports.createUserPre = createUserPre; 
exports.createUserPreOp = createUserPreOp;
exports.findAllUsers = findAllUsers;
exports.deleteUsers = deleteUsers;
exports.findAllUsersByDates = findAllUsersByDates; 
exports.findOneUser = findOneUser; 
exports.findOneUserName = findOneUserName;
exports.findOneUserbyuserName = findOneUserbyuserName;
exports.findOneUserLastName = findOneUserLastName;
exports.updateUser = updateUser;
exports.deleteUserByUsername = deleteUserByUsername;
exports.deleteAllUsers = deleteAllUsers;
exports.findAllUsersByCreatedDate = findAllUsersByCreatedDate;
exports.ValidateOneUser = ValidateOneUser;
exports.findOneUserNamed = findOneUserNamed;
exports.findOneUserLastNamed = findOneUserLastNamed;
exports.findOneUserbyuserNamed= findOneUserbyuserNamed;