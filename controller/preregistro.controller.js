


const dbManager = require ('../database/db.manager');

/**
 * PREREGISTRO CONTROLADOR
 *  
 */



function crearusuario(req,res){
    // verificar que el req no este vacio
    if(!req.body){
        res.status(400).send(
            {

            message: "Request esta vacio"

             }
        );
        return;
        
    }
    // crear el objeto usuario
    const newUserObject ={
        idUser: req.body.idUser,
        nombreUsuario: req.body.nombreUsuario,
        contrasena: req.body.contrasena,
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        correo: req.body.correo,
        Rol: req.body.Rol
    }
    // Insertando en la abse de datos el objeto
    dbManager.Preregistro.create(newUserObject).then(
        data => {
            res.send(data);
        }

    ).catch(
        error => {
            console.log(error);
            res.status (500).send(
                {  

                    message: " ERRRRRROOOOOOOOOORRRRRRRR" + error+
                    data

                }

            );
        }    
        
        
    );

}



// TRAE TODOS LOS USUARIOS DEL PREREGISTRO
async function findallusersprereg(req,res){
    try{const allusers = await dbManager.Preregistro.findAll();

        res.send(
            {
                data: allusers
            }
        );
    }catch(error){
        console.log(error)
        res.status(500).send(
            {
                message:"ERRORR,LO SENTIMOS ESTAMOS TRABAJANDO PARA SOLUCIONARLO"
            }
        )
    }
}

async function DeleteByid(req,res) {
    const { idUser} = req.params;
    
   await dbManager.Preregistro.destroy({
        where: {
            idUser : idUser
        }
     })
     res.send(
         {
             message: "Eliminado"
         }
     )
    }




exports.crearusuario=crearusuario;
exports.findallusersprereg= findallusersprereg;
exports.DeleteByid = DeleteByid;