var express = require('express');
var router = express.Router();
const PerUserController = require ('../controller/preusuario.controler'); 

/**
 * POST Route to create user
 */
router.post ('/createPreUser',PerUserController.createUserPre);
/**
 * options Route to create user
 */
//router.options ('/createPreUser',PerUserController.createUserPreOp);

/**
 * GET Route to get users
 */
router.get ('/findall',PerUserController.findAllUsers);
/**
 * DELETE Route to delete user by id
 */
router.delete ('/delete',PerUserController.deleteUsers);




/**
 * TASK:
 * ADD THE MISSING ROUTES ______________________________________________________ 
 */

// Export router
module.exports = router;

