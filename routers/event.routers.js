var express = require('express');
var router = express.Router();
const rolController = require ('../controller/event.controler'); 

/**
 * POST Route to create event
 */
router.post ('/create',rolController.createEve);

/**
 * GET Route to select event
 */
router.get ('/get',rolController.get);
/**
 * GET Route to select event
 */
router.get ('/getall',rolController.getall);

/**
 * POST Route to create event
 */
router.post ('/createlink',rolController.createLink);

/**
 * TASK:
 * ADD THE MISSING ROUTES ______________________________________________________ 
 */

// Export router
module.exports = router;

