var express = require('express');
var router = express.Router();
const rolController = require ('../controller/rol.controller'); 

/**
 * POST Route to create user
 */
router.post ('/createRol',rolController.createRol);

/**
 * POST Route to select user
 */
router.get ('/selectRol',rolController.selectRol);

/**
 * TASK:
 * ADD THE MISSING ROUTES ______________________________________________________ 
 */

// Export router
module.exports = router;

