var express = require('express');
var router = express.Router();
const rolController = require ('../controller/poster.controller'); 

/**
 * POST Route to create event
 */
router.post ('/create',rolController.createEve);

/**
 * GET Route to select event
 */
router.get ('/get',rolController.get);
/**
 * GET Route to select event
 */
router.get ('/getall',rolController.getall);


/**
 * TASK:
 * ADD THE MISSING ROUTES ______________________________________________________ 
 */

// Export router
module.exports = router;

