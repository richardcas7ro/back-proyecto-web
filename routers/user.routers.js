var express = require('express');
var router = express.Router();
const userController = require ('../controller/user.controller'); 


/**
 * POST Route to create user relation with poster
 */
router.post ('/createUserP',userController.createUserP);

/**
 * POST Route to create user
 */
router.post ('/createUser',userController.createUser);
/**
 * Get Route to login
 */
router.get('/loginU',userController.loginU);
/**
 * GET Route to list all users
 */
router.get('/findByDate', userController.findAllUsersByDates);

/**
 * GET Route to find user by username
 */
router.get('/name', userController.findOneUserName);

/**
 * GET Route to find user by name
 */
router.get('/username', userController.findOneUserbyuserName);

/**
 * GET Route to find user by lastname
 */
router.get('/lastname', userController.findOneUserLastName);


/**
 * GET Route to find user by username and date
 */
router.get('/named', userController.findOneUserNamed);

/**
 * GET Route to find user by name and date
 */
router.get('/usernamed', userController.findOneUserbyuserNamed);

/**
 * GET Route to find user by id and date
 */
router.get('/lastnamed', userController.findOneUserLastNamed);


/**
 * GET Route to find user by id ************************************
 */
router.get('/:idUser', userController.findOneUser);



/**
 * PUT Route to update an user by id
 */
router.put ('/:idUser',userController.updateUser);
/**
 * DELETE Route to delete an user by username
 */
router.delete ('/:username',userController.deleteUserByUsername);
/**
 * DELETE Route to delete all users
 */
//router.delete ('/',userController.deleteAllUsers);
/**
 * Get Route to validate a username
 */
router.get('/ValidateOneUser/:username',userController.ValidateOneUser);



/**
 * TASK:
 * ADD THE MISSING ROUTES ______________________________________________________ 
 */

// Export router
module.exports = router;

